<?php

/**
 * Fields category name
 */
define('BACKLINK_HANDLER_FIELDS_CATEGORY', 'Referrer data');

/**
 * Ip field name
 */
define('BACKLINK_HANDLER_IP_FIELD', 'profile_ip');

/**
 * Lead source field name
 */
define('BACKLINK_HANDLER_LEAD_SOURCE_FIELD', 'profile_lead_source');

/**
 * Current page field name
 */
define('BACKLINK_HANDLER_CURRENT_PAGE_FIELD', 'profile_current_page');

/**
 * Referrer field name
 */
define('BACKLINK_HANDLER_REFERRER_FIELD', 'profile_referrer');

/**
 * Ip field title
 */
define('BACKLINK_HANDLER_IP_TITLE', 'Ip');

/**
 * Lead source field title
 */
define('BACKLINK_HANDLER_LEAD_SOURCE_TITLE', 'Lead source');

/**
 * Current page field title
 */
define('BACKLINK_HANDLER_CURRENT_PAGE_TITLE', 'Current page');

/**
 * Referrer field title
 */
define('BACKLINK_HANDLER_REFERRER_TITLE', 'Referrer');
