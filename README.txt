
This module saves referrer, IP and domain, and initial page the user arrived on from a users initial connection inside the users profile.

To install, place the entire module folder into your modules directory.
Go to administer -> site building -> modules and enable the Backlink Handler module.

4 profile fields will be created during module installation. 

The module doesn't require any settings. You can use the created profile fields just like any other profile fields.